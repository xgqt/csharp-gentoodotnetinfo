# C#-GentooDotnetInfo

.NET information tool for Gentoo


## License

Copyright (c) 2023, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later

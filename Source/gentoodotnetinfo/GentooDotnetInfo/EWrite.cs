/*
 * This file is part of csharp-gentoodotnetinfo - .NET information tool for Gentoo.
 * Copyright (c) 2023, Maciej Barć <xgqt@riseup.net>
 * Licensed under the GNU GPL v2 License
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * csharp-gentoodotnetinfo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * csharp-gentoodotnetinfo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with csharp-gentoodotnetinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;

namespace com.xgqt.GentooDotnetInfo
{
    public static class EWrite
    {
        /// <summary>
        /// Gentoo way of writing generic messages.
        /// </summary>
        /// <seealso cref="einfo"/>
        /// <param name="consoleColor">color to use</param>
        /// <param name="aString">string to write</param>
        public static void ewrite(ConsoleColor consoleColor, string aString)
        {
            if (!Console.IsOutputRedirected)
            {
                Console.ForegroundColor = consoleColor;
            }

            Console.Write(" * ");
            Console.ResetColor();
            Console.WriteLine(aString);
        }

        /// <summary>
        /// Gentoo way of writing information messages.
        /// </summary>
        /// <seealso cref="ewrite"/>
        /// <param name="aString">string to write</param>
        public static void einfo(string aString)
        {
            ewrite(ConsoleColor.Green, aString);
        }
    }
}

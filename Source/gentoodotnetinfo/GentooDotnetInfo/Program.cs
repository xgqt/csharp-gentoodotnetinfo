/*
 * This file is part of csharp-gentoodotnetinfo - .NET information tool for Gentoo.
 * Copyright (c) 2023, Maciej Barć <xgqt@riseup.net>
 * Licensed under the GNU GPL v2 License
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * csharp-gentoodotnetinfo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * csharp-gentoodotnetinfo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with csharp-gentoodotnetinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace com.xgqt.GentooDotnetInfo
{
    public class GentooDotnetInfo
    {
        public static int Main(string[] arg)
        {
            EWrite.einfo("Operating System:");
            EWrite.einfo($"  Process Architecture: {RuntimeInformation.ProcessArchitecture}");
            EWrite.einfo($"  OS Version: {Environment.OSVersion.ToString()}");

            EWrite.einfo("Dotnet SDK:");
            EWrite.einfo($"  Version: {Environment.Version.ToString()}");
            EWrite.einfo($"  Framework Description: {RuntimeInformation.FrameworkDescription}");

            EWrite.einfo("Dotnet Runtime:");
            EWrite.einfo($"  Process Name: {Process.GetCurrentProcess().ProcessName}");
            EWrite.einfo($"  Process File Name: {Process.GetCurrentProcess().MainModule.FileName}");
            EWrite.einfo($"  Runtime Directory: {RuntimeEnvironment.GetRuntimeDirectory()}");

            return 0;
        }
    }
}
